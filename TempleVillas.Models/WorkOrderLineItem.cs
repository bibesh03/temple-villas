﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TempleVillas.Models
{
    public class WorkOrderLineItem
    {
        public int Id { get; set; }
        [ForeignKey("WorkOrder")]
        public int WorkOrderId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Quantity { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Cost { get; set; }

        public WorkOrder WorkOrder { get; set; }
    }
}
