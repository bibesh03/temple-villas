﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Models
{
    public class ReportServer
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
