﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TempleVillas.Models
{
    public class Report
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ReportPath { get; set; }
        [ForeignKey("ReportServer")]
        public int ReportServerId { get; set; }
        public ReportServer ReportServer { get; set; }
    }
}
