﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TempleVillas.Models
{
    public class WorkOrderAttachment
    {
        public int Id { get; set; }
        [ForeignKey("WorkOrder")]
        public int WorkOrderId { get; set; }
        public string FileName { get; set; }
        public Guid Guid { get; set; }
        public string Extension { get; set; }
        public string FileLocation { get; set; }
        [ForeignKey("User")]
        public int UploadedBy { get; set; }
        public DateTime UploadedDate { get; set; }

        public User User { get; set; }
        public WorkOrder WorkOrder { get; set; }
    }
}
