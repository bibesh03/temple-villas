﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TempleVillas.Models
{
    public class Room
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [ForeignKey("UnitId")]
        public int UnitId { get; set; }

        public Unit Unit { get; set; }
    }
}
