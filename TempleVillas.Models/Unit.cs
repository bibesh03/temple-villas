﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TempleVillas.Models
{
    public class Unit
    {
        public int Id { get; set; }
        [ForeignKey("Location")]
        public int LocationId { get; set; }
        public string UnitName { get; set; }

        public Location Location { get; set; }

        public ICollection<Room> Rooms { get; set; }
    }
}
