﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TempleVillas.Models
{
    public class Technician
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int? UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal PayRate { get; set; }

        public User User { get; set; }
        public ICollection<WorkOrder> WorkOrders { get; set; }
    }
}
