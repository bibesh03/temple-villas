﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TempleVillas.Models
{
    public class WorkOrder
    {
        public int Id { get; set; }
        [ForeignKey("Technician")]
        public int TechnicianId { get; set; }
        [ForeignKey("Room")]
        public int? RoomId { get; set; }
        public DateTime? DateStarted { get; set; }
        public DateTime? DateCompleted { get; set; }
        public string PropertyWearId { get; set; }
        [ForeignKey("User")]
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsCompleted { get; set; }
        public string Description { get; set; }

        public DateTime? WorkOrderCompletedDateTime { get; set; }
        public Technician Technician { get; set; }
        public Room Room { get; set; }
        public User User { get; set; }

        public ICollection<WorkOrderLineItem> WorkOrderLineItems { get; set; }
        public ICollection<WorkOrderAttachment> WorkOrderAttachments { get; set; }
    }
}
