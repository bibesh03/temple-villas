﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Models
{
    public class Location
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Unit> Units { get; set; }
    }
}
