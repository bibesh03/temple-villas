﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using TempleVillas.Models;

namespace TempleVillas.Data
{
    public class DataContext : DbContext
    {
       
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorkOrder>()
                .HasOne(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.CreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<WorkOrderAttachment>()
                .HasOne(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UploadedBy)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<WorkOrder>()
                .Property(x => x.Description)
                .HasDefaultValue("");
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Technician> Technicians { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<WorkOrder> WorkOrders { get; set; }
        public DbSet<WorkOrderLineItem> WorkOrderLineItems { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportServer> ReportServers { get; set; }
        public DbSet<WorkOrderAttachment> WorkOrderAttachments { get; set; }

    }
}
