﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TempleVillas.Models;

namespace TempleVillas.Data
{
    public static class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            if (context.Users.Any())
            {
                return;
            }

            var salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            var users = new User[]
            {
                new User { IsActive = true, Email = "templevillas@gmail.com", IsUserAdmin = true, FirstName = "Temple", LastName = "Villas", UserName = "wilbertFuneral", Password=HashPassword("templeP@$$", salt), PasswordSalt = Convert.ToBase64String(salt)},
                new User { IsActive = true, Email = "bibesh@primtek.net", IsUserAdmin = true, FirstName = "Bibesh", LastName = "KC", UserName = "bibesh07", Password=HashPassword("pr1mt3kP@$$", salt), PasswordSalt = Convert.ToBase64String(salt)}
            };

            foreach (var user in users)
            {
                context.Users.Add(user);
            }

            context.SaveChanges();
        }

        private static string HashPassword(string password, byte[] salt) => Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));
    }
}
