﻿using System;
using System.Collections.Generic;
using System.Text;
using TempleVillas.Dto.Location;
using TempleVillas.Models;

public interface ILocationService
{
    Location GetLocationById(int id);
    ICollection<LocationListingDto> GetLocations();
    void SaveLocation(Location Location);
    void DeleteLocation(int LocationId);
}
