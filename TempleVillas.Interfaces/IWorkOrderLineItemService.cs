﻿using System;
using System.Collections.Generic;
using System.Text;
using TempleVillas.Models;

namespace TempleVillas.Interface
{
    public interface IWorkOrderLineItemService
    {
        WorkOrderLineItem GetWorkOrderLineItemById(int id);
        ICollection<WorkOrderLineItem> GetWorkOrderLineItems();
        void SaveWorkOrderLineItem(WorkOrderLineItem workOrderLineItem);
        void DeleteWorkOrderLineItem(int WorkOrderLineItemId);
    }
}
