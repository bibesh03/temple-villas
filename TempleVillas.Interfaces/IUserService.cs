﻿using System;
using System.Collections.Generic;
using System.Text;
using TempleVillas.Dto.User;
using TempleVillas.Models;

public interface IUserService
{
    User GetUserById(int id);
    int DoesUserNameOrEmailExist(string email, string username);
    List<User> GetAllUsers();
    AuthenticateResponseDto Authenticate(AuthenticateRequestDto model);
    void RegisterUser(RegisterUserDto userToSave);
    void ChangePassword(int userId, string password);
    void SaveUser(RegisterUserDto userToSave);
    ICollection<UserForDropDownDto> GetNonAdminUsers();
}
