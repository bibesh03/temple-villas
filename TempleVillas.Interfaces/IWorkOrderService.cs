﻿using System;
using System.Collections.Generic;
using System.Text;
using TempleVillas.Dto.WorkOrder;
using TempleVillas.Models;

namespace TempleVillas.Interface
{
    public interface IWorkOrderService
    {
        WorkOrder GetWorkOrderById(int id);
        ICollection<WorkOrderListingDto> GetWorkOrders();
        void SaveWorkOrder(WorkOrder workOrder);
        void DeleteWorkOrder(int workOrderId);
    }
}
