﻿using System;
using System.Collections.Generic;
using System.Text;
using TempleVillas.Models;

namespace TempleVillas.Interface
{
    public interface IWorkOrderAttachmentService
    {
        ICollection<WorkOrderAttachment> GetAttachmentsByWorkOrder(int workOrderId);
        void SaveAttachment(WorkOrderAttachment attachment);
        void DeleteAttachment(Guid guid);
    }
}
