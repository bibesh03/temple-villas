﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Interface
{
    public interface IPrintService
    {
        byte[] GetFileFromServer(string url, string userName, string password);
    }
}
