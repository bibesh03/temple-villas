﻿using System;
using System.Collections.Generic;
using System.Text;
using TempleVillas.Models;

public interface ITechnicianService
{
    Technician GetTechnicianByUserId(int userId);
    Technician GetTechnicianById(int id);
    ICollection<Technician> GetTechnicians();
    void SaveTechnician(Technician technician);
    void DeleteTechnician(int technicianId);
}
