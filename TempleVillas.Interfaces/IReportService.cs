﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Interface
{
    public interface IReportService
    {
        byte[] GetWorkOrderPrintOut(int workOrderId);
    }
}
