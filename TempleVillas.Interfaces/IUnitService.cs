﻿using System;
using System.Collections.Generic;
using System.Text;
using TempleVillas.Dto.Unit;
using TempleVillas.Models;

public interface IUnitService
{
    Unit GetUnitById(int id);
    ICollection<Unit> GetUnits();
    void SaveUnit(Unit Unit);
    void DeleteUnit(int UnitId);
}
