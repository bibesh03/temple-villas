﻿using System;
using System.Collections.Generic;
using System.Text;
using TempleVillas.Dto.Room;
using TempleVillas.Models;

public interface IRoomService
{
    Room GetRoomById(int id);
    ICollection<Room> GetRooms();
    void SaveRoom(Room Room);
    void DeleteRoom(int RoomId);
    ICollection<RoomListingDto> GetRoomsForDropDown();
}
