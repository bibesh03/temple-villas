﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Interface;
using TempleVillas.Models;

namespace TempleVillas.Services
{
    public class WorkOrderLineItemService : IWorkOrderLineItemService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public WorkOrderLineItemService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public WorkOrderLineItem GetWorkOrderLineItemById(int id) => context.WorkOrderLineItems.FirstOrDefault(x => x.Id == id);

        public ICollection<WorkOrderLineItem> GetWorkOrderLineItems() => context.WorkOrderLineItems.ToList();

        public void SaveWorkOrderLineItem(WorkOrderLineItem WorkOrderLineItem)
        {
            if (WorkOrderLineItem.Id == 0)
                context.WorkOrderLineItems.Add(WorkOrderLineItem);
            else
            {
                var WorkOrderLineItemToEdit = GetWorkOrderLineItemById(WorkOrderLineItem.Id);
                mapper.Map(WorkOrderLineItem, WorkOrderLineItemToEdit);
            }

            context.SaveChanges();
        }

        public void DeleteWorkOrderLineItem(int workOrderLineItemId)
        {
            context.WorkOrderLineItems.Remove(GetWorkOrderLineItemById(workOrderLineItemId));
            context.SaveChanges();
        }
    }
}
