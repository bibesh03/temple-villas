﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Dto.WorkOrder;
using TempleVillas.Interface;
using TempleVillas.Models;

namespace TempleVillas.Services
{
    public class WorkOrderService : IWorkOrderService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public WorkOrderService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public WorkOrder GetWorkOrderById(int id) => context.WorkOrders.Include(x => x.Technician).ThenInclude(x => x.User).Include(x => x.WorkOrderLineItems).FirstOrDefault(x => x.Id == id);

        public ICollection<WorkOrderListingDto> GetWorkOrders()
        {
            List<WorkOrderListingDto> listingDto = new List<WorkOrderListingDto>();
            var workOrder = context.WorkOrders.Include(x => x.WorkOrderLineItems).Include(x => x.Technician).Include(x => x.Room)
                            .ThenInclude(x => x.Unit).ThenInclude(x => x.Location).OrderByDescending(x => x.CreatedDate).ToList();
            mapper.Map(workOrder, listingDto);

            return listingDto;
        }

        public void SaveWorkOrder(WorkOrder WorkOrder)
        {
            if (WorkOrder.Id == 0)
                context.WorkOrders.Add(WorkOrder);
            else
            {
                var WorkOrderToEdit = GetWorkOrderById(WorkOrder.Id);
                mapper.Map(WorkOrder, WorkOrderToEdit);
            }

            context.SaveChanges();
        }

        public void DeleteWorkOrder(int WorkOrderId)
        {
            context.WorkOrders.Remove(GetWorkOrderById(WorkOrderId));
            context.SaveChanges();
        }
    }
}
