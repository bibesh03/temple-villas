﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Dto.Room;
using TempleVillas.Models;

namespace TempleVillas.Services
{
    public class RoomService : IRoomService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public RoomService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Room GetRoomById(int id) => context.Rooms.FirstOrDefault(x => x.Id == id);

        public ICollection<Room> GetRooms() => context.Rooms.ToList();

        public void SaveRoom(Room Room)
        {
            if (Room.Id == 0)
                context.Rooms.Add(Room);
            else
            {
                var RoomToEdit = GetRoomById(Room.Id);
                mapper.Map(Room, RoomToEdit);
            }

            context.SaveChanges();
        }

        public void DeleteRoom(int RoomId)
        {
            context.Rooms.Remove(GetRoomById(RoomId));
            context.SaveChanges();
        }

        public ICollection<RoomListingDto> GetRoomsForDropDown() => context.Rooms.Select(x => new RoomListingDto
        {
            Id = x.Id,
            Name = x.Unit.Location.Name + " - " + x.Unit.UnitName + " - " + x.Name 
        }).ToList();
    }
}
