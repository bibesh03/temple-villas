﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Dto.Unit;
using TempleVillas.Models;

namespace TempleVillas.Services
{
    public class UnitService : IUnitService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public UnitService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Unit GetUnitById(int id) => context.Units.FirstOrDefault(x => x.Id == id);

        public ICollection<Unit> GetUnits() => context.Units.Include(x => x.Rooms).ToList();

        public void SaveUnit(Unit Unit)
        {
            if (Unit.Id == 0)
                context.Units.Add(Unit);
            else
            {
                var UnitToEdit = GetUnitById(Unit.Id);
                mapper.Map(Unit, UnitToEdit);
            }

            context.SaveChanges();
        }

        public void DeleteUnit(int UnitId)
        {
            context.Units.Remove(GetUnitById(UnitId));
            context.SaveChanges();
        }
    }
}
