﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Interface;

namespace TempleVillas.Services
{
    public class ReportService : IReportService
    {
        private readonly DataContext context;
        private readonly IPrintService printService;

        public ReportService(DataContext _context, IPrintService _printService)
        {
            this.context = _context;
            this.printService = _printService;
        }

        public byte[] GetWorkOrderPrintOut(int workOrderId)
        {
            var server = context.ReportServers.FirstOrDefault();
            if (server != null)
            {
                var report = context.Reports.FirstOrDefault(x => x.Name == "WorkOrderPrintOut" && x.ReportServerId == server.Id);
                if (report != null)
                {
                    var url = string.Format("{0}{1}&workOrderId={2}", server.Url.Trim(), report.ReportPath.Trim(), workOrderId);

                    return printService.GetFileFromServer(url, server.UserName, server.Password);
                }
            }

            return null;
        }
    }
}
