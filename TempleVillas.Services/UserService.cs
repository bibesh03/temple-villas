﻿using AutoMapper;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Dto.User;
using TempleVillas.Models;

namespace TempleVillas.Services
{
    public class UserService : IUserService
    {
        private readonly DataContext context;
        public readonly IConfiguration configuration;
        private readonly IMapper mapper;
        private readonly ITechnicianService technicianService;

        public UserService(DataContext _context, IConfiguration _configuration, IMapper _mapper, ITechnicianService _technicianService)
        {
            this.context = _context;
            this.configuration = _configuration;
            this.mapper = _mapper;
            this.technicianService = _technicianService;
        }

        public User GetUserById(int id) => context.Users.FirstOrDefault(x => x.Id == id);

        public int DoesUserNameOrEmailExist(string email, string username) => context.Users.Where(x => x.Email == email || x.UserName == username).Count();

        public List<User> GetAllUsers() => context.Users.ToList();

        public ICollection<UserForDropDownDto> GetNonAdminUsers() {
            var allEligibleUsers = context.Users.Where(x => x.IsActive == true && x.IsUserAdmin == false).Select(x => new UserForDropDownDto
            {
                Id = x.Id,
                Name = x.FirstName + " " + x.LastName
            }).ToList();

            var allTechnicians = technicianService.GetTechnicians();
            List <UserForDropDownDto> filteredList = new List<UserForDropDownDto>();
            if (allTechnicians != null)
            {
                filteredList = allTechnicians.Select(x => new UserForDropDownDto
                {
                    Id = x.UserId ?? 0,
                    Name = x.User.FirstName + " " + x.User.LastName
                }).ToList();
            }
            
            return allEligibleUsers.Where(x => filteredList.All(a => a.Id != x.Id)).ToList();
        }

        public AuthenticateResponseDto Authenticate(AuthenticateRequestDto model)
        {
            var user = context.Users.FirstOrDefault(x => (x.UserName == model.Email || x.Email == model.Email) && x.IsActive == true);
            if (user == null)
            {
                return null;
            }
            if (user.Password != HashPassword(model.Password, Convert.FromBase64String(user.PasswordSalt)))
            {
                return null;
            }

            var jwtToken = GenerateJWTToken(user);
            return new AuthenticateResponseDto(user, jwtToken);
        }

        public void RegisterUser(RegisterUserDto userToSave)
        {
            var user = mapper.Map<User>(userToSave);
            SavePasword(user, userToSave.Password);
            context.Users.Add(user);

            context.SaveChanges();
        }

        public void SaveUser(RegisterUserDto userToSave)
        {
            var user = GetUserById(userToSave.Id);
            mapper.Map(userToSave, user);

            context.SaveChanges();
        }

        public void ChangePassword(int userId, string password)
        {
            var userToSave = GetUserById(userId);
            SavePasword(userToSave, password);
        }

        private void SavePasword(User user, string password)
        {
            var salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            user.Password = HashPassword(password, salt);
            user.PasswordSalt = Convert.ToBase64String(salt);

            context.SaveChanges();
        }

        private string HashPassword(string password, byte[] salt) => Convert.ToBase64String(KeyDerivation.Pbkdf2(
                                                                                password: password,
                                                                                salt: salt,
                                                                                prf: KeyDerivationPrf.HMACSHA1,
                                                                                iterationCount: 10000,
                                                                                numBytesRequested: 256 / 8));

        private string GenerateJWTToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(configuration["JwtSecretKey"]);
            var technician = technicianService.GetTechnicianByUserId(user.Id);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserId", user.Id.ToString()),
                        new Claim(ClaimTypes.Role, user.IsUserAdmin ? "Admin" : context.Technicians.Where(x => x.UserId == user.Id).Any() ? "Technician" : ""),
                        new Claim("TechnicianId", technician == null ? "" : technician.Id.ToString())
                    }),
                Expires = DateTime.UtcNow.AddDays(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
