﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Interface;
using TempleVillas.Models;

namespace TempleVillas.Services
{
    public class WorkOrderAttachmentService : IWorkOrderAttachmentService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public WorkOrderAttachmentService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public ICollection<WorkOrderAttachment> GetAttachmentsByWorkOrder(int workOrderId) => context.WorkOrderAttachments.Where(x => x.WorkOrderId == workOrderId).ToList();
 
        public void SaveAttachment(WorkOrderAttachment attachment)
        {
            context.WorkOrderAttachments.Add(attachment);
            context.SaveChanges();
        }

        public void DeleteAttachment(Guid guid)
        {
            var attachmentToDelete = context.WorkOrderAttachments.FirstOrDefault(x => x.Guid == guid);
            if (attachmentToDelete != null)
            {
                context.WorkOrderAttachments.Remove(attachmentToDelete);
                context.SaveChanges();
            }
        }
    }
}
