﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Dto.Location;
using TempleVillas.Models;

namespace TempleVillas.Services
{
    public class LocationService : ILocationService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public LocationService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Location GetLocationById(int id) => context.Locations.FirstOrDefault(x => x.Id == id);

        public ICollection<LocationListingDto> GetLocations() => context.Locations.Select(x => new LocationListingDto{
            Id = x.Id,
            Name = x.Name,
            Count = x.Units.Count()
        }).ToList();

        public void SaveLocation(Location Location)
        {
            if (Location.Id == 0)
                context.Locations.Add(Location);
            else
            {
                var LocationToEdit = GetLocationById(Location.Id);
                mapper.Map(Location, LocationToEdit);
            }

            context.SaveChanges();
        }

        public void DeleteLocation(int LocationId)
        {
            context.Locations.Remove(GetLocationById(LocationId));
            context.SaveChanges();
        }
    }
}
