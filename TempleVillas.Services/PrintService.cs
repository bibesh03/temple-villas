﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using TempleVillas.Interface;

namespace TempleVillas.Services
{
    public class PrintService : IPrintService
    {
        public byte[] GetFileFromServer(string url, string userName, string password)
        {
            var request = WebRequest.Create(url);

            request.Method = "GET";

            request.Credentials = new NetworkCredential(userName, password);

            var response = request.GetResponse();

            var copyOf = new MemoryStream();

            using (var originalStream = response.GetResponseStream())
            {
                originalStream.CopyTo(copyOf);
            }

            var bytes = copyOf.ToArray();

            copyOf.Dispose();

            return bytes;
        }
    }
}
