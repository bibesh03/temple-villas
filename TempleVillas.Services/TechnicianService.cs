﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TempleVillas.Data;
using TempleVillas.Models;

namespace TempleVillas.Services
{
    public class TechnicianService : ITechnicianService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public TechnicianService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Technician GetTechnicianByUserId(int userId) => context.Technicians.FirstOrDefault(x => x.UserId == userId);

        public Technician GetTechnicianById(int id) => context.Technicians.Include(x => x.User).FirstOrDefault(x => x.Id == id);

        public ICollection<Technician> GetTechnicians() => context.Technicians.Include(x => x.User).ToList();

        public void SaveTechnician(Technician technician)
        {
            if (technician.Id == 0)
                context.Technicians.Add(technician);
            else
            {
                var technicianToEdit = GetTechnicianById(technician.Id);
                mapper.Map(technician, technicianToEdit);
            }

            context.SaveChanges();
        }

        public void DeleteTechnician(int technicianId)
        {
            context.Technicians.Remove(GetTechnicianById(technicianId));
            context.SaveChanges();
        }
    }
}
