﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.User
{
    public class UserForDropDownDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
