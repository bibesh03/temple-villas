﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.User
{
    public class RegisterUserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public bool IsUserAdmin { get; set; }
        public bool IsActive { get; set; }
    }
}
