﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.Room
{
    public class RoomListingDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
