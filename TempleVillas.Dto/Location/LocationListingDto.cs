﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.Location
{
    public class LocationListingDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
