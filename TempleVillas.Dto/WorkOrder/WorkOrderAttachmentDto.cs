﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.WorkOrder
{
    public class WorkOrderAttachmentDto
    {
        public int CreatedBy { get; set; }
        public int WorkOrderId { get; set; }
        public IFormFile File { get; set; }
    }
}
