﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.WorkOrder
{
    public class WorkOrderListingDto
    {
        public int Id { get; set; }
        public string WorkOrderId { get; set; }
        public string Location { get; set; }
        public string DateStarted { get; set; }
        public string DateCompleted { get; set; }
        public string Technician { get; set; }
        public string CompletedDate { get; set; }
        public string Rate { get; set; }
        public string PropertyWearId { get; set; }
        public int TechnicianUserId { get; set; }
        public int TechnicianId { get; set; }
        public int? RoomId { get; set; }
        public bool IsCompleted { get; set; }
        public string Description { get; set; }
    }
}
