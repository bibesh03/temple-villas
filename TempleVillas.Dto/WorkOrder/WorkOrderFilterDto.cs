﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.WorkOrder
{
    public class WorkOrderFilterDto
    {
        public int[] TechnicianIds { get; set; }
        public int[] RoomIds{ get; set; }
        public string DateRange { get; set; }
    }
}
