﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.Technician
{
    public class TechnicianDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public decimal PayRate { get; set; }
    }
}
