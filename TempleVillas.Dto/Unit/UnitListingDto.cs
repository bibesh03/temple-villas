﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TempleVillas.Dto.Unit
{
    public class UnitListingDto
    {
        public int Id { get; set; }
        public string UnitName { get; set; }
        public string Location { get; set; }
    }
}
