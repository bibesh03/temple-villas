﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TempleVillas.Models;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public class UnitController : ControllerBase
    {
        private readonly IUnitService UnitService;
        public UnitController(IUnitService _UnitService)
        {
            this.UnitService = _UnitService;
        }

        [HttpGet("GetAllUnits")]
        public IActionResult GetAllUnits()  =>  Ok(UnitService.GetUnits());

        [HttpGet("GetAllUnitsByLocation/{locationId}")]
        public IActionResult GetAllUnitsByLocation(int locationId) => Ok(UnitService.GetUnits().Where(x => x.LocationId == locationId).ToList());

        [HttpGet("DeleteUnit/{UnitId}")]
        public IActionResult DeleteUnit(int UnitId)
        {
            UnitService.DeleteUnit(UnitId);
            return Ok(new { Message = "Unit has been deleted" });
        }

        [HttpGet("Edit/{UnitId}")]
        public IActionResult Edit(int UnitId) => Ok(UnitService.GetUnitById(UnitId));

        [HttpPost("Edit")]
        public IActionResult SaveUnit([FromBody] Unit Unit)
        {
            UnitService.SaveUnit(Unit);

            return Ok(new { Message = "Success! " + Unit.UnitName + " has been updated." });
        }
    }
}
