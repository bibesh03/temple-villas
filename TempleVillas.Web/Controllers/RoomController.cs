﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TempleVillas.Models;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public class RoomController : ControllerBase
    {
        private readonly IRoomService roomService;
        public RoomController(IRoomService _roomService)
        {
            this.roomService = _roomService;
        }

        [HttpGet("GetAllRooms")]
        public IActionResult GetAllRooms() => Ok(roomService.GetRooms());

        [HttpGet("DeleteRoom/{roomId}")]
        public IActionResult DeleteRoom(int roomId)
        {
            roomService.DeleteRoom(roomId);
            return Ok(new { Message = "Room has been deleted" });
        }

        [HttpGet("Edit/{RoomId}")]
        public IActionResult Edit(int roomId) => Ok(roomService.GetRoomById(roomId));

        [HttpPost("Edit")]
        public IActionResult SaveRoom([FromBody] Room room)
        {
            roomService.SaveRoom(room);

            return Ok(new { Message = "Success! " + room.Name + " has been updated." });
        }

        [AllowAnonymous]
        [HttpGet("GetRoomsForDropDown")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Technician")]
        public IActionResult GetRoomsForDropDown() => Ok(roomService.GetRoomsForDropDown());
    }
}
