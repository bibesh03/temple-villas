﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using TempleVillas.Interface;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ReportController : ControllerBase
    {
        public readonly IReportService reportService;

        public ReportController(IReportService _reportService)
        {
            this.reportService = _reportService;
        }

        private bool IsAuthorized(string jwtToken)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(jwtToken);
                var tokenS = handler.ReadToken(jwtToken) as JwtSecurityToken;

                return tokenS.Claims.First(claim => claim.Type == "role").Value != null;
            } catch (Exception ex)
            {
                return false;
            }
        }

        [AllowAnonymous]
        [HttpGet("GetWorkOrderPrintOut")]
        public ActionResult GetWorkOrderPrintOut(int workOrderId, string token)
        {
            if (IsAuthorized(token)) {
                var lotoToPrint = reportService.GetWorkOrderPrintOut(workOrderId);
                return File(lotoToPrint, "application/pdf", "WorkOrder_" + (workOrderId + "").PadLeft(7, '0') + ".pdf");
            }

            return null;
        }
    }
}
