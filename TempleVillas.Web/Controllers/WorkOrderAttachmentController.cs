﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TempleVillas.Dto.WorkOrder;
using TempleVillas.Interface;
using TempleVillas.Models;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class WorkOrderAttachmentController : ControllerBase
    {
        private IWorkOrderAttachmentService workOrderAttachmentService;
        private readonly string CurrentDirectory = Path.Combine(Environment.CurrentDirectory, "Attachments");

        public WorkOrderAttachmentController(IWorkOrderAttachmentService _workOrderAttachmentService)
        {
            this.workOrderAttachmentService = _workOrderAttachmentService;
        }

        [HttpPost("SaveAttachment")]
        public IActionResult SaveAttachment([FromForm] WorkOrderAttachmentDto workOrderAttachments)
        {
            var fileGuid = Guid.NewGuid();
            var fileExtension = Path.GetExtension(workOrderAttachments.File.FileName);
            var filePath = Path.Combine(CurrentDirectory, fileGuid + fileExtension);

            if (UploadFileToServer(filePath, workOrderAttachments.File))
            {
                WorkOrderAttachment fileToSave = new WorkOrderAttachment
                {
                    Extension = fileExtension,
                    FileName = workOrderAttachments.File.FileName,
                    Guid = fileGuid,
                    UploadedDate = DateTime.Now,
                    WorkOrderId = workOrderAttachments.WorkOrderId,
                    FileLocation = "/Attachments/" + fileGuid + fileExtension,
                    UploadedBy = workOrderAttachments.CreatedBy,
                };
                workOrderAttachmentService.SaveAttachment(fileToSave);
            }
             
            return Ok(new { Message = "Attachments has been successfully uploaded" });
        }

        private bool UploadFileToServer(string filePath, IFormFile file)
        {
            try
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpGet("GetAllAttachmentsByWorkOrder")]
        public IActionResult GetAllAttachmentsByWorkOrder(int workOrderId) => Ok(workOrderAttachmentService.GetAttachmentsByWorkOrder(workOrderId));

        [HttpDelete("DeleteAttachment")]
        public IActionResult DeleteAttachment(Guid guid)
        {
            workOrderAttachmentService.DeleteAttachment(guid);

            return Ok(new { Message = "Attachment has been successfully deleted" });
        }
    }
}
