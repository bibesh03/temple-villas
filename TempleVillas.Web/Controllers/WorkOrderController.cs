﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TempleVillas.Dto.WorkOrder;
using TempleVillas.Interface;
using TempleVillas.Models;
using TempleVillas.Web.Extensions;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class WorkOrderController : ControllerBase
    {
        private readonly IWorkOrderService workOrderService;
        private string ROLE_KEY = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";

        public WorkOrderController(IWorkOrderService _workOrderService)
        {
            this.workOrderService = _workOrderService;
        }

        [HttpGet("GetAllWorkOrders")]
        public IActionResult GetAllWorkOrders() => Ok(workOrderService.GetWorkOrders());

        [HttpGet("GetAllWorkOrdersByType/{userId}/{completed}")]
        public IActionResult GetAllWorkOrdersForUserByType(int userId, bool completed) => Ok(workOrderService.GetWorkOrders().Where(x => x.TechnicianUserId == userId && x.IsCompleted == completed).ToList());

        [HttpGet("DeleteWorkOrder/{WorkOrderId}")]
        public IActionResult DeleteWorkOrder(int workOrderId)
        {
            workOrderService.DeleteWorkOrder(workOrderId);
            return Ok(new { Message = "WorkOrder has been deleted" });
        }

        [HttpGet("Edit/{WorkOrderId}")]
        public IActionResult Edit(int workOrderId)
        {
            var workOrder = workOrderService.GetWorkOrderById(workOrderId);
            if (GetJwtPayLoad(ROLE_KEY) != "Admin")
            {
                if (Int32.Parse(GetJwtPayLoad("UserId")) != workOrder.Technician.UserId)
                    throw new HttpException(HttpStatusCode.Forbidden, "You are not allowed to view work order assigned to other technicians");
            }

            return Ok(workOrder);
        }

        [HttpPost("Edit")]
        public IActionResult SaveWorkOrder([FromBody] WorkOrder workOrder)
        {
            workOrderService.SaveWorkOrder(workOrder);

            return Ok(new { Message = "Success! Work Order: #" + (workOrder.Id + "").PadLeft(7, '0') + " has been updated.", Id = workOrder.Id });
        }

        private string GetJwtPayLoad(string key)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity.Claims.Count() != 0)
            {
                IEnumerable<Claim> claims = identity.Claims;
                return identity.FindFirst(key).Value;
            }

            return "";
        }

        [HttpPost("Filter")]
        public IActionResult Filter([FromBody] WorkOrderFilterDto workOrderFilter)
        {
            var allWorkOrders = workOrderService.GetWorkOrders();

            if (workOrderFilter.TechnicianIds.Count() > 0)
            {
                allWorkOrders = allWorkOrders.Where(x => workOrderFilter.TechnicianIds.Contains(x.TechnicianId)).ToList();
            }
            if (workOrderFilter.RoomIds.Count() > 0)
            {
                allWorkOrders = allWorkOrders.Where(x => workOrderFilter.RoomIds.Contains(x.RoomId ?? 0)).ToList();
            }
            if (!String.IsNullOrEmpty(workOrderFilter.DateRange))
            {
                var dateArr = workOrderFilter.DateRange.Split(" - ");
                allWorkOrders = allWorkOrders.Where(x => x.DateStarted != "-").Where(x => DateTime.Parse(x.DateStarted) >= DateTime.Parse(dateArr[0])).ToList();
                if (dateArr[1] != "")
                {
                    allWorkOrders = allWorkOrders.Where(x => DateTime.Parse(x.DateStarted) <= DateTime.Parse(dateArr[1])).ToList();
                }
            }

            return Ok(allWorkOrders);
        }
    }
}