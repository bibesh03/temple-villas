﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TempleVillas.Interface;
using TempleVillas.Models;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class WorkOrderLineItemController : ControllerBase
    {
        private readonly IWorkOrderLineItemService workOrderLineItemService;
        public WorkOrderLineItemController(IWorkOrderLineItemService _workOrderLineItemService)
        {
            this.workOrderLineItemService = _workOrderLineItemService;
        }

        [HttpGet("GetAllWorkOrderLineItems")]
        public IActionResult GetAllWorkOrderLineItems() => Ok(workOrderLineItemService.GetWorkOrderLineItems());

        [HttpGet("DeleteWorkOrderLineItem/{workOrderLineItemId}")]
        public IActionResult DeleteWorkOrderLineItem(int workOrderLineItemId)
        {
            workOrderLineItemService.DeleteWorkOrderLineItem(workOrderLineItemId);
            return Ok(new { Message = "Item has been deleted" });
        }

        [HttpGet("Edit/{workOrderLineItemId}")]
        public IActionResult Edit(int workOrderLineItemId) => Ok(workOrderLineItemService.GetWorkOrderLineItemById(workOrderLineItemId));

        [HttpPost("Edit")]
        public IActionResult SaveWorkOrderLineItem([FromBody] WorkOrderLineItem workOrderLineItem)
        {
            workOrderLineItemService.SaveWorkOrderLineItem(workOrderLineItem);

            return Ok(new { Message = "Success! " + workOrderLineItem.Name + " has been updated." });
        }
    }
}
