﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using TempleVillas.Dto.User;
using TempleVillas.Web.Extensions;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public class UsersController : ControllerBase
    {
        private IUserService userService;
        private string ROLE_KEY = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
        public UsersController(IUserService _userService)
        {
            this.userService = _userService;
        }

        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateRequestDto model)
        {
            var response = userService.Authenticate(model);
            if (response == null)
                throw new HttpException(HttpStatusCode.BadRequest, "Username or password is incorrect");

            return Ok(response);
        }

        [HttpPost("Register")]
        public IActionResult Register([FromBody] RegisterUserDto user)
        {
            if (userService.DoesUserNameOrEmailExist(user.Email, user.UserName) > 0)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Email or username already exist. Please use unique Email and Username.");
            }
            userService.RegisterUser(user);

            return Ok(new { message = "Success! " + user.UserName + " has been Registered." });
        }

        [HttpGet("GetAllUser")]
        public IActionResult GetAllUser() => Ok(userService.GetAllUsers());

        [AllowAnonymous]
        [HttpPost("Edit")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Technician")]
        public IActionResult Edit([FromBody] RegisterUserDto user)
        {
            if (userService.DoesUserNameOrEmailExist(user.Email, user.UserName) > 1)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Email or username already exist. Please use unique Email and Username.");
            }
            userService.SaveUser(user);

            return Ok(new { Message = "Success! " + user.UserName + " has been updated." });
        }

        private string GetJwtPayLoad(string key)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity.Claims.Count() != 0)
            {
                IEnumerable<Claim> claims = identity.Claims;
                return identity.FindFirst(key).Value;
            }

            return "";
        }

        [AllowAnonymous]
        [HttpPost("ChangePassword/{userId}/{password}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Technician")]
        public IActionResult ChangePassword(int userId, string password)
        {
            if (GetJwtPayLoad(ROLE_KEY) != "Admin")
            {
                if (Int32.Parse(GetJwtPayLoad("UserId")) != userId)
                    throw new HttpException(HttpStatusCode.Forbidden, "You are not allowed to change password for this user");
            }
            password = password == "**" ? "templeP@$$" : password;
            userService.ChangePassword(userId, password);
            return Ok(new { Message = "Success! Password has been updated. The updated password is: " + password });
        }

        [AllowAnonymous]
        [HttpGet("Edit/{userId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Technician")]
        public IActionResult Edit(int userId) {
            if (GetJwtPayLoad(ROLE_KEY) != "Admin")
            {
                if (Int32.Parse(GetJwtPayLoad("UserId")) != userId)
                    throw new HttpException(HttpStatusCode.Forbidden, "You are not allowed to change user details for this user");
            }

            return Ok(userService.GetUserById(userId));
        }

        [HttpGet("GetNonAdminUsers")]
        public IActionResult GetNonAdminUsers(int userId) => Ok(userService.GetNonAdminUsers());
    }
}
