﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TempleVillas.Models;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public class LocationController : ControllerBase
    {
        private readonly ILocationService LocationService;
        public LocationController(ILocationService _LocationService)
        {
            this.LocationService = _LocationService;
        }

        [HttpGet("GetAllLocations")]
        public IActionResult GetAllLocations() => Ok(LocationService.GetLocations());

        [HttpGet("DeleteLocation/{LocationId}")]
        public IActionResult DeleteLocation(int LocationId)
        {
            LocationService.DeleteLocation(LocationId);
            return Ok(new { Message = "Location has been deleted" });
        }

        [HttpGet("Edit/{LocationId}")]
        public IActionResult Edit(int LocationId) => Ok(LocationService.GetLocationById(LocationId));

        [HttpPost("Edit")]
        public IActionResult SaveLocation([FromBody] Location Location)
        {
            LocationService.SaveLocation(Location);

            return Ok(new { Message = "Success! " + Location.Name + " has been updated." });
        }
    }
}
