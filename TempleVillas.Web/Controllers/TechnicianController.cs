﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TempleVillas.Models;

namespace TempleVillas.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public class TechnicianController : ControllerBase
    {
        private readonly ITechnicianService technicianService;
        public TechnicianController(ITechnicianService _technicianService)
        {
            this.technicianService = _technicianService;
        }

        [AllowAnonymous]
        [HttpGet("GetAllTechnicians")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Technician")]
        public IActionResult GetAllTechnicians() => Ok(technicianService.GetTechnicians());

        [HttpGet("DeleteTechnician/{technicianId}")]
        public IActionResult DeleteTechnician(int technicianId)
        {
            technicianService.DeleteTechnician(technicianId);
            return Ok( new { Message = "Technician has been deleted" });
        }

        [HttpGet("Edit/{technicianId}")]
        public IActionResult Edit(int technicianId) => Ok(technicianService.GetTechnicianById(technicianId));

        [HttpPost("Edit")]
        public IActionResult SaveTechnician([FromBody] Technician technician)
        {
            technicianService.SaveTechnician(technician);

            return Ok(new { Message = "Success! " + technician.FirstName + "'s profile has been updated." });
        }
    }
}
