﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TempleVillas.Dto.User;
using TempleVillas.Dto.WorkOrder;
using TempleVillas.Models;

namespace TempleVillas.Web.Helpers.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //User Mapping
            CreateMap<User, RegisterUserDto>()
                .ForMember(x => x.Password, src => src.Ignore())
                .ForMember(x => x.ConfirmPassword, src => src.Ignore()).ReverseMap();

            //Technician
            CreateMap<Technician, Technician>()
                .ForMember(x => x.User, src => src.Ignore());

            //Location
            CreateMap<Location, Location>()
                .ForMember(x => x.Units, src => src.Ignore());

            //Unit
            CreateMap<Unit, Unit>();

            //Room
            CreateMap<Room, Room>();

            //WorkOrder
            CreateMap<WorkOrder, WorkOrder>()
                .ForMember(x => x.Room, src => src.Ignore())
                .ForMember(x => x.User, src => src.Ignore())
                .ForMember(x => x.Technician, src => src.Ignore())
                .ForMember(x => x.WorkOrderLineItems, src => src.Ignore());

            CreateMap<WorkOrder, WorkOrderListingDto>()
                .ForMember(x => x.WorkOrderId, src => src.MapFrom(a => a.Id.ToString().PadLeft(7, '0')))
                .ForMember(x => x.Technician, src => src.MapFrom(a => a.Technician != null ? a.Technician.FirstName + " " + a.Technician.LastName : ""))
                .ForMember(x => x.DateStarted, src => src.MapFrom(a => a.DateStarted == null ? "-" : a.DateStarted.Value.ToString("MM/dd/yyyy hh:mm tt")))
                .ForMember(x => x.DateCompleted, src => src.MapFrom(a => a.DateCompleted == null ? "-" : a.DateCompleted.Value.ToString("MM/dd/yyyy hh:mm tt")))
                .ForMember(x => x.CompletedDate, src => src.MapFrom(a => a.WorkOrderCompletedDateTime == null ? "-" : a.WorkOrderCompletedDateTime.Value.ToString("MM/dd/yyyy hh:mm tt")))
                .ForMember(x => x.Location, src => src.MapFrom(a => a.RoomId == null ? "-" : a.Room.Unit.Location.Name + " - " + a.Room.Unit.UnitName + " - " + a.Room.Name))
                .ForMember(x => x.TechnicianUserId, src => src.MapFrom(a => a.Technician.UserId))
                .ForMember(x => x.TechnicianId, src => src.MapFrom(a => a.Technician.Id))
                .ForMember(x => x.Rate, src => src.MapFrom(a => a.Technician.PayRate));

            //WorkOrderLineItem
            CreateMap<WorkOrderLineItem, WorkOrderLineItem>();

            //WorkOrderAttachments
            CreateMap<WorkOrderAttachment, WorkOrderAttachment>();
        }
    }
}
