﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TempleVillas.Web.Migrations
{
    public partial class Removedratefromworkorder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rate",
                table: "WorkOrders");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Rate",
                table: "WorkOrders",
                type: "decimal(18,2)",
                nullable: true);
        }
    }
}
