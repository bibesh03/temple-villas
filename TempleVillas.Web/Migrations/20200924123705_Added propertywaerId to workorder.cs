﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TempleVillas.Web.Migrations
{
    public partial class AddedpropertywaerIdtoworkorder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PropertyWearId",
                table: "WorkOrders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PropertyWearId",
                table: "WorkOrders");
        }
    }
}
