﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TempleVillas.Web.Migrations
{
    public partial class Changedroomidtonullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkOrders_Rooms_RoomId",
                table: "WorkOrders");

            migrationBuilder.AlterColumn<int>(
                name: "RoomId",
                table: "WorkOrders",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkOrders_Rooms_RoomId",
                table: "WorkOrders",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkOrders_Rooms_RoomId",
                table: "WorkOrders");

            migrationBuilder.AlterColumn<int>(
                name: "RoomId",
                table: "WorkOrders",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkOrders_Rooms_RoomId",
                table: "WorkOrders",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
